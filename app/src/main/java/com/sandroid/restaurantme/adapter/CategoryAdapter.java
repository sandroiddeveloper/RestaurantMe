package com.sandroid.restaurantme.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Parcelable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.sandroid.restaurantme.FoodList;
import com.sandroid.restaurantme.Home;
import com.sandroid.restaurantme.Interface.ItemClickListener;
import com.sandroid.restaurantme.Model.Category;
import com.sandroid.restaurantme.R;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by sandroid on 2/8/18.
 */

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.MenuViewHolder> {

    Context context;
    List<Category> list;

    public CategoryAdapter(Context context, List<Category> list) {
        this.list = list;
        this.context = context;
    }

    @Override
    public MenuViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.menu_item, parent, false);
        return new MenuViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final MenuViewHolder holder, int position) {

        final Category data= list.get(position);
        holder.txtMenuName.setText(data.getName());
        Picasso.with(context).load(data.getImage()).into(holder.imageView);
        final Class<Category> ClickItem = Category.class;
        holder.setItemClickListener(new ItemClickListener() {
            @Override
            public void onClick(View view, int position, boolean isLongClick) {
                Intent intent=new Intent(context,FoodList.class);
                intent.putExtra("menuId",position);
                context.startActivity(intent);
                Toast.makeText(context, "position :"+position, Toast.LENGTH_SHORT).show();
            }
        });
    }


    @Override
    public int getItemCount() {
        return list.size();
    }


    class MenuViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView txtMenuName;
        public ImageView imageView;

        private ItemClickListener itemClickListener;

        public MenuViewHolder(View itemView) {
            super(itemView);

            txtMenuName = itemView.findViewById(R.id.menu_name);
            imageView = itemView.findViewById(R.id.image_menu);

            itemView.setOnClickListener(this);
        }

        public void setItemClickListener(ItemClickListener itemClickListener) {
            this.itemClickListener = itemClickListener;
        }

        @Override
        public void onClick(View view) {
            itemClickListener.onClick(view, getAdapterPosition(), false);
        }
    }
}


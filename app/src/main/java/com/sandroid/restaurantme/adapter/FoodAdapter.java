package com.sandroid.restaurantme.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.sandroid.restaurantme.Interface.ItemClickListener;
import com.sandroid.restaurantme.Model.Category;
import com.sandroid.restaurantme.Model.Food;
import com.sandroid.restaurantme.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class FoodAdapter extends RecyclerView.Adapter<FoodAdapter.FoodViewHolder>{

    private final Context context;
    private final List<Food> foodLists;

    public FoodAdapter(Context context, List<Food>FoodLists) {

        this.context = context;
        foodLists = FoodLists;
    }

    @Override
    public FoodViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.food_item,parent,false);
        return new FoodViewHolder(view);
    }

    @Override
    public void onBindViewHolder(FoodViewHolder holder, int position) {
        Food data= foodLists.get(position);
        holder.txtFoodName.setText(data.getName());
        Picasso.with(context).load(data.getImage()).into(holder.imageFood);
        Class<Food>ClickItem=Food.class;
        holder.setItemClickListener(new ItemClickListener() {
            @Override
            public void onClick(View view, int position, boolean isLongClick) {

            }
        });

    }

    @Override
    public int getItemCount() {
        return foodLists.size();
    }


    public class FoodViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        public TextView txtFoodName;
        public ImageView imageFood;
        private ItemClickListener itemClickListener;

        public FoodViewHolder(View itemView) {
            super(itemView);

            txtFoodName = itemView.findViewById(R.id.food_name);
            imageFood = itemView.findViewById(R.id.image_Food);

            itemView.setOnClickListener( this);
        }
        public void setItemClickListener(ItemClickListener itemClickListener) {
            this.itemClickListener = itemClickListener;
        }


        @Override
        public void onClick(View view) {
            itemClickListener.onClick(view,getAdapterPosition(),false);
        }
    }
}

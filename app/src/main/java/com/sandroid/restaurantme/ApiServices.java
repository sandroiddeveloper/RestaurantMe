//package com.sandroid.restaurantme;
//
//import android.content.Context;
//import android.util.Log;
//
//import com.android.volley.DefaultRetryPolicy;
//import com.android.volley.Request;
//import com.android.volley.Response;
//import com.android.volley.VolleyError;
//import com.android.volley.toolbox.JsonArrayRequest;
//import com.android.volley.toolbox.Volley;
//import com.sandroid.restaurantme.Model.Category;
//
//import org.json.JSONArray;
//import org.json.JSONException;
//import org.json.JSONObject;
//
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
///**
// * Created by sandroid on 2/14/18.
// */
//
//public class ApiServices {
//    private final Context context;
//    private static final String DB_URL = "http://androidamooz.com/sadegh/DatabaseManager.php";
//    private String TAG = "apiService";
//    private String id;
//    private String name;
//    private String image_url;
//
//    public ApiServices(Context context) {
//        this.context = context;
//    }
//
//    public void getCategory(final OnCategoryReceived onCategoryReceived) {
//        JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET,
//                DB_URL, null, new Response.Listener<JSONArray>() {
//            @Override
//            public void onResponse(JSONArray response) {
//                Log.d("Response :", "" + response);
//
//
//                List<Category> lists = new ArrayList<>();
//                Category category;
//                for (int i = 0; i < response.length(); i++) {
//
//                    try {
//                        //  JSONArray array = new JSONArray(response);
//
//                        //response khodesh ye array hast lazem nist bezari to array mesle code bala
//                        JSONObject jsonObject = (JSONObject) response.get(i);
//                        id =(jsonObject.getString("id"));
//                        name = (jsonObject.getString("name"));
//                        image_url = (jsonObject.getString("image_url"));
//                        category = new Category(id,name,image_url);
//                        lists.add(category);
//
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                        Log.i(TAG, "onResponse: " + response);
//                    }
//                }
//                onCategoryReceived.onReceived(lists);
//
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                Log.e(TAG, "onErrorResponse: ",error );
//            }
//
//        }) {
//            @Override
//            protected Map<String, String> getParams() {
//                Map<String, String> params = new HashMap<String, String>();
//                params.put("id", id);
//                params.put("name", name);
//                params.put("image_url", image_url);
//                return params;
//            }
//        };
//        request.setRetryPolicy(new DefaultRetryPolicy(
//                18000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
//                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
//
//        Volley.newRequestQueue(context).add(request);
//    }
//
//    public interface OnCategoryReceived {
//        void onReceived(List<Category> lists);
//    }
//}

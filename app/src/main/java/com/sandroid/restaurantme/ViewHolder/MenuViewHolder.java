//package com.sandroid.restaurantme.ViewHolder;
//
//import android.support.v7.widget.RecyclerView;
//import android.view.View;
//import android.widget.ImageView;
//import android.widget.TextView;
//
//import com.sandroid.restaurantme.Interface.ItemClickListener;
//import com.sandroid.restaurantme.R;
//
///**
// * Created by sandroid on 2/7/18.
// */
//
//public class MenuViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
//    public TextView txtMenuName;
//    public ImageView imageView;
//
//    private ItemClickListener itemClickListener;
//    public MenuViewHolder(View itemView) {
//        super(itemView);
//
//        txtMenuName=(TextView)itemView.findViewById(R.id.menu_name);
//        imageView=(ImageView) itemView.findViewById(R.id.image_menu);
//
//        itemView.setOnClickListener(this);
//    }
//
//    public void setItemClickListener(ItemClickListener itemClickListener) {
//        this.itemClickListener = itemClickListener;
//    }
//
//    @Override
//    public void onClick(View view) {
//        itemClickListener.onClick(view,getAdapterPosition(),false);
//
//    }
//}

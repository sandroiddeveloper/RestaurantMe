package com.sandroid.restaurantme;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class SignUp extends AppCompatActivity {

    private TextInputLayout inputUserName, inputPhoneNumber, inputPassword;
    private EditText edtPassword, edtName, edtPhonumber;
    private Button btnSignUp;

    private static final String REGISTER_URL="http://androidamooz.com/sadegh/Register.php";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        inputPhoneNumber = findViewById(R.id.input_phone_number);
        inputUserName = findViewById(R.id.input_user_name);
        inputPassword = findViewById(R.id.input_password);

        edtName = findViewById(R.id.edtName);
        edtPassword = findViewById(R.id.edtPasword);
        edtPhonumber = findViewById(R.id.edtPhone);

        btnSignUp = findViewById(R.id.btnSignUp);
        btnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                registerUser();
            }
        });


    }

    private void registerUser() {
        String username=edtName.getText().toString().trim().toLowerCase();
        String phone=edtPhonumber.getText().toString().trim().toLowerCase();
        String password=edtPassword.getText().toString().trim().toLowerCase();
        register(username,phone,password);
    }
    private void register(String username, String phone, String password) {
        String urlSufix="?username= "+username+" & phone= "+phone+" & password= "+password;
        class registerUser extends AsyncTask<String,Void,String>{
            ProgressDialog loding;

            @Override
            protected void onPreExecute(){
                super.onPreExecute();
                loding=ProgressDialog.show(SignUp.this,"لطفا صبور باشید...",null,true,true);
            }

            @Override
            protected void onPostExecute(String s){
                super.onPostExecute(s);
                loding.dismiss();
                if (doInBackground() !=null){
                    startActivity(new Intent(SignUp.this,MainActivity.class));
                }
            }

            @Override
            protected String doInBackground(String... params) {
                String s = params[0];
                BufferedReader bufferedReader=null;
                try{
                    URL url = new URL(REGISTER_URL+s);
                    HttpURLConnection connect=(HttpURLConnection)url.openConnection();
                    bufferedReader=new BufferedReader(new InputStreamReader(connect.getInputStream()));
                    String result;
                    result=bufferedReader.readLine();

                    return result;
                }catch (Exception e){
                    return null;
                }
            }
        }
        registerUser user = new registerUser();
        user.execute(urlSufix);
    }
}

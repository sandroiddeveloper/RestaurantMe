package com.sandroid.restaurantme.Model;

/**
 * Created by sandroid on 2/1/18.
 */

public class User {
    private String Name;
    private String Pasword;

    public User() {
    }

    public User(String name, String pasword) {
        Name = name;
        Pasword = pasword;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getPasword() {
        return Pasword;
    }

    public void setPasword(String pasword) {
        Pasword = pasword;
    }
}

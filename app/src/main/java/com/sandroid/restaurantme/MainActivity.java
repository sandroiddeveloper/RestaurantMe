package com.sandroid.restaurantme;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button signIn = findViewById(R.id.btnSign_In);
        Button signUp = findViewById(R.id.btnSign_Up);
        ImageView logo = findViewById(R.id.logo);

        TextView sandroid = findViewById(R.id.txtSandroid);
        Typeface type = Typeface.createFromAsset(getAssets(), "fonts/khandevane.ttf");
        sandroid.setTypeface(type);

        signIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, Home.class));
            }
        });

        signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, SignUp.class));
            }
        });

    }

}

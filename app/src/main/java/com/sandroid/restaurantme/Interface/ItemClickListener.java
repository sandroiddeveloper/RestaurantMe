package com.sandroid.restaurantme.Interface;

import android.view.View;

/**
 * Created by sandroid on 2/5/18.
 */

public interface ItemClickListener {
    void onClick(View view,int position,boolean isLongClick);
}

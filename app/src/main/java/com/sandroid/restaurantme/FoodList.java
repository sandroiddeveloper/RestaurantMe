package com.sandroid.restaurantme;

import android.content.Context;
import android.content.Intent;
import android.database.DatabaseUtils;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Layout;
import android.util.Log;
import android.widget.Toolbar;


import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.common.data.DataBufferUtils;
import com.sandroid.restaurantme.Model.Category;
import com.sandroid.restaurantme.Model.Food;
import com.sandroid.restaurantme.adapter.CategoryAdapter;
import com.sandroid.restaurantme.adapter.FoodAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class FoodList extends AppCompatActivity {
    List<Food> foodList=new ArrayList<>();

    private static final String DB_URL = "http://androidamooz.com/sadegh/Food.php?cat=";
    private String TAG;
    String menuId;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_food_list);
        getIncomingIntent();
    }
    private void getIncomingIntent(){
        Intent intent=this.getIntent();
            menuId= intent.getExtras().getString("menuId");
            int menuId=intent.getExtras().getInt("menuId");
            loadListFood(menuId);
    }
    private void loadListFood(int menuId) {
        JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET,
                DB_URL+menuId, null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                Log.d("Response :", "" + response);

                List<Food> foodlist = new ArrayList<>();

                for (int i = 1; i < response.length(); i++) {
                    int p = response.length();
                    Food food= new Food();

                    try {
//                        JSONArray array = new JSONArray(response);
                        JSONObject jsonObject = (JSONObject) response.get(i);
                        food.setId(jsonObject.getString("id"));
                        food.setName(jsonObject.getString("name"));
                        food.setImage(jsonObject.getString("image"));
                        food.setDescription(jsonObject.getString("description"));
                        food.setDiscount(jsonObject.getString("discount"));
                        food.setPrice(jsonObject.getString("price"));
                        food.setMenuId(jsonObject.getString("menuId"));

                        foodList.add(food);


                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.i(TAG, "onResponse: " + response);
                    }
                }
                //load menu
                FoodAdapter foodAdapter = new FoodAdapter(FoodList.this, foodList);
                RecyclerView recyclerList = findViewById(R.id.recycler_foodList);
                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(FoodList.this);
                linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                recyclerList.setLayoutManager(linearLayoutManager);
                recyclerList.setHasFixedSize(true);
                recyclerList.setAdapter(foodAdapter);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "onErrorResponse: ", error);
            }

        }) {
//            @Override
//            protected Map<String, String> getParams() {
//                Map<String, String> params = new HashMap<String, String>();
//                params.put("id", id);
//                params.put("name", name);
//                params.put("image_url", image_url);
//                return params;
//            }
        };
        request.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 50000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 50000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });
        Volley.newRequestQueue(FoodList.this).add(request);
    }


}
